<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Belajar PHP - Fungsi Kondisi</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3><mark> Jawaban - Soal No 1 Greetings </mark></h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini
function greetings($strNama){
    echo "Halo <em><b>$strNama</b></em>, Selamat Datang di Sanbercode ! <br>";
}

// Hapus komentar untuk menjalankan code!

greetings("Bagas");
greetings("Wahyu");
greetings("Hakiki");
echo "<hr color=black size 2px>";



echo "<h3><mark> Jawaban - Soal No 2 Reverse String </mark></h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini
function balikString($kata1){
    $pParameter = strlen($kata1);
    $nampung    = "";
    for($index=$pParameter-1; $index>=0; $index--){
        $nampung .= $kata1[$index];
    }
    return $nampung;
}
function reverseString($kata2){
    $balikan = balikString($kata2);
    echo $balikan . "<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
reverseString("Hakiki");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<hr color=black size 2px>";


echo "<h3><mark> Jawaban - Soal No 3 Palindrome </mark></h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


// Code function di sini
function palindrome($pali){
    $balik = balikString($pali);
    if($pali === $balik){
        echo "$pali => <b> TRUE </b><br>";
    }else{
        echo"$pali => <b> FALSE </b><br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true
echo "<hr color=black size 2px>";


echo "<h3><mark> Soal No 4 Tentukan Nilai </h3></mark>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($poin){
    if($poin>=85 && $poin<=100){
        return "$poin => Sangat Baik <br>";
    }else if($poin>=70 && $poin<85){
        return "$poin => Baik <br>";
    }else if($poin>=60 && $poin<70){
        return "$poin => Cukup <br>";
    }else {
        return "$poin => Kurang <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
echo "<hr color=black size 2px>";


?>

</body>

</html>