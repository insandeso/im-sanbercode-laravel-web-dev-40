
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Latihan PHP - Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> <mark> Jawaban Soal No.1 </mark></h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
    $kids       = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"]; // Lengkapi di sini
    $adults     = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    
    echo "<b>   Kids : </b></em><br>";
        echo"<ul>";
            echo "<li>"; print_r($kids); echo "</li>";
        echo "</ul>";

    echo "<b>Adults : </b><br>";
    echo"<ul>";
        echo "<li>"; print_r($adults); echo "</li>";
    echo "</ul>";
    echo "<hr color=black>";



    echo "<h3> <mark> Jawaban Soal No.2 </mark></h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "<em> Cast Stranger Things : </em><br>";
    echo "<br>";
    echo "<b>   Total Kids : </b>".count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
        echo "<li> $kids[0] </li>"; //Lanjutkan
        echo "<li> $kids[1] </li>";
        echo "<li> $kids[2] </li>";
        echo "<li> $kids[3] </li>";
        echo "<li> $kids[4] </li>";
        echo "<li> $kids[5] </li>"; 
    echo "</ol>";

    echo "<b>   Total Adults  : </b>".count($adults); // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
        echo "<li> $adults[0] </li>"; // Lanjutkan
        echo "<li> $adults[1] </li>";
        echo "<li> $adults[2] </li>";
        echo "<li> $adults[3] </li>";
    echo "</ol>";
    echo "<hr color=black>";


    echo "<h3> <mark> Jawaban Soal No.3 </mark></h3>";
    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"


            Output:
            Array
                (
                    [0] => Array
                        (
                            [Name] => Will Byers
                            [Age] => 12
                            [Aliases] => Will the Wise
                            [Status] => Alive
                        )

                    [1] => Array
                        (
                            [Name] => Mike Wheeler
                            [Age] => 12
                            [Aliases] => Dugeon Master
                            [Status] => Alive
                        )

                    [2] => Array
                        (
                            [Name] => Jim Hooper
                            [Age] => 43
                            [Aliases] => Chief Hopper
                            [Status] => Deceased
                        )

                    [3] => Array
                        (
                            [Name] => Eleven
                            [Age] => 12
                            [Aliases] => El
                            [Status] => Alive
                        )

                )
            
        */
        $multidimensi = 
        [
            ["NAME"=>"Will Byers", "AGE"=>"12", "ALIASES"=>"Will the Wise", "STATUS"=>"Alive"],

            ["NAME"=>"Mike Wheeler", "AGE"=>"12", "ALIASES"=>"Dungeon Master", "STATUS"=>"Alive"],

            ["NAME"=>"Jim Hopper", "AGE"=>"43", "ALIASES"=>"Chief Hopper", "STATUS"=>"Deceased"],

            ["NAME"=>"Eleven", "AGE"=>"12", "ALIASES"=>"El", "STATUS"=>"Alive"]

        ];
        
        echo "<em> Data Casting Stranger Things : </em><br>";
        echo "<pre>";
        print_r($multidimensi);
        echo"</pre>";
        echo "<br>";

    ?>
</body>

</html>