
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3>Soal No 1</h3>";
        /* 
        SOAL NO 1
        Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

        Contoh: 
        $string = "PHP is never old";
        Output:
        Panjang string: 16, 
        Jumlah kata: 4 
        */
        
        $first_sentence = "Hello PHP!"; // Panjang string 10, jumlah kata: 2
        echo "<pre><em> Kalimat 1        </em>: ".$first_sentence."<br>";
        echo "<em> Panjang String   </em>: ".strlen($first_sentence)."<br>";
        echo "<em> Jumlah Kata      </em>: ".str_word_count($first_sentence)."<br><br>";

        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        echo "<em> Kalimat 2        </em>: ".$second_sentence."<br>";
        echo "<em> Panjang String   </em>: ".strlen($second_sentence)."<br>";
        echo "<em> Jumlah Kata      </em>: ".str_word_count($second_sentence)."<br><br></pre><hr size=3px color=black>";
        
        
        
        
        echo "<h3> Soal No 2</h3>";
        /* 
        SOAL NO 2
        Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
        
        
        */
        $string2 = "I love PHP";
        
        echo "<label>String : </label> \"$string2\" <br>";
        echo "<pre><em> Kata Pertama    </em>:".substr($string2, 0, 1)."<br>" ; 
        echo "<em> Kata Kedua      </em>:".substr($string2, 2, 4)."<br>";
        echo "<em> Kata Ketiga     </em>:".substr($string2, 7, 3)."<br><br></pre><hr size=3px color=black>" ;

        
        
        
        echo "<h3> Soal No 3 </h3>";
        /*
        SOAL NO 3
        Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "<pre> String                :  \"$string3\" "; 
        // OUTPUT : "PHP is old but awesome"
        echo "<br> Hasil Pergantian Kata :  ".str_replace("sexy!", "AWESOME !", "\"$string3\"")."</pre>";

    ?>
</body>
</html>