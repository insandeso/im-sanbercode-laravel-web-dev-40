<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function sending(Request $request)
    {
        $namaDepan      = $request ['first_name'];
        $namaBelakang   = $request ['last_name'];

        return view ('halaman.welcome', 
            [
                'namaDepan' => $namaDepan,
                'namaBelakang' => $namaBelakang
            ]);
    }
}
