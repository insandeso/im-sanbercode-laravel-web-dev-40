<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>- WELCOME -</title>
</head>
<body>
    <h2>SELAMAT DATANG !</h2>
    <h3>Terima kasih <i>{{$namaDepan}} {{$namaBelakang}}</i> telah bergabung di Sanberbook. Social Media kita bersama !</h3>

    <p>Kembali ke <a href="/">Beranda</a>

</body>
</html>