<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>- REGISTER - </title>
</head>
<body>
    <h1>Buat Account Baru !</h1>
    <form action="/kirimkan" method="POST">
        @csrf
        <h2>Sign Up Form</h2>

        <label>First Name :</label><br>
            <input type="text" name="first_name"><br>
        <br>

        <label>Last Name :</label><br>
            <input type="text" name="last_name"><br>
        <br>

        <label>Gender :</label><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br>
        <br>

        <label>Nationality :</label><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="england">England</option>
            <option value="japan">Japan</option>
            <option value="usa">USA</option>
            <option value="brazil">Brazil</option>
        </select><br>
        <br>
        
        <label>Language Spoken :</label><br>
            <input type="checkbox" name="lan_spoken">Bahasa Indonesia<br>
            <input type="checkbox" name="lan_spoken">English<br>
            <input type="checkbox" name="lan_spoken">Other<br>
        <br>
        
        <label>Bio :</label><br>
            <textarea name="text_bio" cols="20" rows="5"></textarea><br>
        
        <input type="submit" value="kirim"><br>
        <br>

        <label>Kembali ke <a href="/">Beranda</a> </label><br>
        <br>
    </form>
</body>
</html>